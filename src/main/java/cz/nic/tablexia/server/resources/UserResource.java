package cz.nic.tablexia.server.resources;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.util.JSONPObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

import cz.nic.tablexia.server.dao.CustomAvatarDAO;
import cz.nic.tablexia.server.dao.UserDAO;
import cz.nic.tablexia.shared.model.CustomAvatarDataPacket;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GamePause;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.Screen;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.UserDifficultySettings;
import cz.nic.tablexia.shared.model.definitions.GameDefinition;
import cz.nic.tablexia.shared.rest.UserRestPath;
import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;

import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
@Path(UserRestPath.USER_PATH)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {
    private static final Logger LOG = LoggerFactory.getLogger(UserResource.class);

    private final UserDAO userDAO;
    private final CustomAvatarDAO avatarDAO;

    public UserResource(UserDAO userDAO, CustomAvatarDAO avatarDAO) {
        this.userDAO = userDAO;
        this.avatarDAO = avatarDAO;
    }

    @POST
    @Path(UserRestPath.USER_CREATE)
    @Timed
    @PermitAll
    public Response createV321(User user) {
        String newUserUuid;
        User dbUser = userDAO.findUserByUuid(user.getUuid());

        userDAO.begin();

        if (dbUser != null) {
            newUserUuid = user.getUuid();
            userDAO.update(user.getUuid(), user.getName(), user.getSignature(), user.getAge(), user.getGender(), user.getAvatar(), user.isDeleted(), user.isIntro(), user.isHelp());
        } else {
            long userId = userDAO.insert(user.getName(), user.getSignature(), user.getAge(), user.getGender(), user.getAvatar(), user.isDeleted(), user.isIntro(), user.isHelp());
            dbUser = userDAO.findById(userId);
            newUserUuid = dbUser.getUuid();
        }

        if (user.getGames() != null) {
            for (Game game: user.getGames()) {
                long newGameId = userDAO.insertGame(    dbUser.getId(),
                                                        game.getStartTime(),
                                                        game.getEndTime(),
                                                        game.getGameDifficulty(),
                                                        game.getGameNumber(),
                                                        game.getRandomSeed(),
                                                        game.getGameLocale(),
                                                        game.getApplicationVersionName(),
                                                        game.getApplicationVersionCode(),
                                                        game.getModelVersionName(),
                                                        game.getModelVersionCode(),
                                                        game.getBuildType(),
                                                        game.getPlatform(),
                                                        game.getHwSerialNumber(),
                                                        //3.2.1 backward compatibility
                                                        game.getRankSystemVersionCode() == null ? Game.BACK_COMPATIBILITY_321_RANK_SYSTEM_VERSION_CODE : game.getRankSystemVersionCode(),
                                                        game.getGameScoreResolverVersion() == null ? GameDefinition.DEFAULT_GAME_RESOLVER_VERSION : game.getGameScoreResolverVersion(),
                                                        game.getUserRank(),
                                                        game.getUserAge()
                        );

                if (game.getGameScoreMap() != null) {
                    for (GameScore score : game.getGameScoreMap()) {
                        userDAO.insertGameScore(newGameId, score.getKey(), score.getValue());
                    }
                }

                if (game.getGamePauses() != null) {
                    for (GamePause pause : game.getGamePauses()) {
                        userDAO.insertGamePause(newGameId, pause.getStartTime(), pause.getEndTime());
                    }
                }
            }
        }

        if (user.getScreens() != null) {
            for (Screen screen : user.getScreens()) {
                userDAO.insertScreenVisited(dbUser.getId(), screen.getScreenName(), screen.getTime());
            }
        }

        if (user.getDifficultySettings() != null) {
            userDAO.deleteUserDifficultySetting(dbUser.getId());
            for (UserDifficultySettings setting : user.getDifficultySettings()) {
                userDAO.insertUserDifficultySetting(dbUser.getId(), setting.getGameNumber(), setting.getGameDifficulty());
            }
        }

        userDAO.commit();
        return Response.status(Response.Status.CREATED).header(UserRestPath.USER_UUID, newUserUuid).build();
    }

    @POST
    @Path(UserRestPath.USER_CREATE_OLD)
    @Timed
    @PermitAll
    @Deprecated
    public Response create(User user) {
        String newUserUuid;
        User dbUser = userDAO.findUserByUuid(user.getUuid());

        userDAO.begin();

        if (dbUser != null) {
            newUserUuid = user.getUuid();
            userDAO.update(user.getUuid(), user.getName(), user.getSignature(), user.getAge(), user.getGender(), user.getAvatar(), user.isDeleted(), user.isIntro(), user.isHelp());
        } else {
            long userId = userDAO.insert(user.getName(), user.getSignature(), user.getAge(), user.getGender(), user.getAvatar(), user.isDeleted(), user.isIntro(), user.isHelp());
            dbUser = userDAO.findById(userId);
            newUserUuid = dbUser.getUuid();
        }

        if (user.getGames() != null) {
            for (Game game : user.getGames()) {
                long newGameId = userDAO.insertGameOld( dbUser.getId(),
                                                        game.getStartTime(),
                                                        game.getEndTime(),
                                                        game.getGameDifficulty(),
                                                        game.getGameNumber(),
                                                        game.getRandomSeed());

                if (game.getGameScoreMap() != null) {
                    for (GameScore score : game.getGameScoreMap()) {
                        userDAO.insertGameScore(newGameId, score.getKey(), score.getValue());
                    }
                }

                if (game.getGamePauses() != null) {
                    for (GamePause pause : game.getGamePauses()) {
                        userDAO.insertGamePause(newGameId, pause.getStartTime(), pause.getEndTime());
                    }
                }
            }
        }

        if (user.getScreens() != null) {
            for (Screen screen : user.getScreens()) {
                userDAO.insertScreenVisited(dbUser.getId(), screen.getScreenName(), screen.getTime());
            }
        }

        if (user.getDifficultySettings() != null) {
            userDAO.deleteUserDifficultySetting(dbUser.getId());
            for (UserDifficultySettings setting : user.getDifficultySettings()) {
                userDAO.insertUserDifficultySetting(dbUser.getId(), setting.getGameNumber(), setting.getGameDifficulty());
            }
        }

        userDAO.commit();
        return Response.status(Response.Status.CREATED).header(UserRestPath.USER_UUID, newUserUuid).build();
    }

    @POST
    @Path(UserRestPath.USER_CONFIRM)
    @Timed
    @PermitAll
    public Response confirm(@PathParam(UserRestPath.USER_UUID) String uuid) {
        userDAO.confirmUser(uuid);
        return Response.ok().build();
    }

    @POST
    @Path(UserRestPath.USER_GET)
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    public Response getUser(@PathParam(UserRestPath.USER_UUID) String uuid) {
        User user = userDAO.findUserByUuid(uuid);
        if (user != null) {
            user.setGames(userDAO.getAllGamesWithPausesAndScores(user.getId()));
            user.setScreens(userDAO.findScreensByUserId(user.getId()));
            user.setDifficultySettings(userDAO.getAllUserDifficultySetting(user.getId()));
            return Response.ok(user).build();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @DELETE
    @Path(UserRestPath.USER_DELETE)
    @Timed
    @PermitAll
    public Response deleteUser(@PathParam(UserRestPath.USER_UUID) String uuid) {
        User user = userDAO.findUserByUuid(uuid);
        if (user != null) {
            userDAO.markUserDeleted(uuid);
            return Response.ok().build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Path(UserRestPath.DOWNLOAD_AVATAR)
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    public Response downloadAvatar(@PathParam(UserRestPath.USER_UUID) String uuid) {
        User user = userDAO.findUserByUuid(uuid);
        if(user == null) return Response.status(Response.Status.NOT_FOUND).build();

        CustomAvatarDataPacket avatarDataPacket = avatarDAO.getCustomUserAvatar(user.getId());
        if(avatarDataPacket == null) return Response.status(Response.Status.NOT_FOUND).build();

        return Response.ok(avatarDataPacket).build();
    }

    @POST
    @Path(UserRestPath.UPLOAD_AVATAR)
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    public Response uploadAvatar(@PathParam(UserRestPath.USER_UUID) String uuid, CustomAvatarDataPacket avatarDataPacket) {
        User user = userDAO.findUserByUuid(uuid);
        if(user == null) return Response.status(Response.Status.NOT_FOUND).build();

        byte[] avatar = avatarDataPacket.getAvatarData();
        if(avatar == null || avatar.length == 0) return Response.status(Response.Status.BAD_REQUEST).build();

        avatarDAO.deleteCustomUserAvatar(user.getId());

        Long syncAt = System.currentTimeMillis();
        avatarDAO.insertCustomUserAvatar(user.getId(), avatar, syncAt);

        return Response.ok(syncAt).build();
    }

    @POST
    @Path(UserRestPath.AVATAR_ACTUAL)
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    public Response avatarActual(@PathParam(UserRestPath.USER_UUID) String uuid, Long avatarSyncAt) {
        User user = userDAO.findUserByUuid(uuid);
        if(user == null) return Response.status(Response.Status.NOT_FOUND).build();

        CustomAvatarDataPacket customAvatarDataPacket = avatarDAO.getCustomUserAvatar(user.getId());
        if(customAvatarDataPacket != null && customAvatarDataPacket.hasSyncAt()) {
            if(customAvatarDataPacket.getSyncAt() > avatarSyncAt) {
                return Response.ok(false).build();
            } else {
                return Response.ok(true).build();
            }
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }
}