package cz.nic.tablexia.server.resources;

import com.codahale.metrics.annotation.Timed;

import org.eclipse.jetty.io.WriterOutputStream;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import cz.nic.tablexia.server.dao.StatisticsDAO;
import cz.nic.tablexia.server.dao.mappers.GamesExportMapper;
import cz.nic.tablexia.server.model.statistics.FormPreparedValue;
import cz.nic.tablexia.server.model.statistics.FormValues;
import cz.nic.tablexia.server.model.statistics.GeneralInfo;
import cz.nic.tablexia.server.model.statistics.LastGameInfo;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameDefinition;
import cz.nic.tablexia.shared.model.definitions.GenderDefinition;
import cz.nic.tablexia.shared.model.definitions.INumberedDefinition;
import cz.nic.tablexia.shared.model.definitions.LocaleDefinition;
import liquibase.util.csv.CSVWriter;

@Path("/statistics")
public class StatisticsResource {
    private static final String CONTENT_TYPE_KEY                                = "Content-Type";
    private static final String CONTENT_TYPE_VALUE                              = "application/csv";

    private static final String HEADER_CONTENT_DISPOSITION_LABEL                = "Content-Disposition";
    private static final String HEADER_ATTACHMENT_FORMAT                        = "attachment; filename=\"%s\"";
    private static final String EXPORTED_FILE_NAME_FORMAT                       = "games_export_%s.csv";

    private static final String             FORM_ANY_KEY                            = "ANY";
    private static final int                FORM_ANY_VALUE                          = -999;
    private static final FormPreparedValue  FORM_ANY_PREPARED_VALUE                 = new FormPreparedValue(FORM_ANY_KEY, FORM_ANY_VALUE);

    private static final String             LOCALE_DEFINITION_LEGACY_KEYS           = ",0,-1";

    private static final String             SQL_IN_FILTER_CONDITION_FORMAT          = " AND %s IN (%s) ";
    private static final String             SQL_IN_FILTER_VALUE_FORMAT              = "%s";
    private static final String             SQL_IN_FILTER_VALUE_FORMAT_APOSTROPHE   = "\'%s\'";

    private static final String             APP_VERSION_FILTER_CONDITION_VAL_NAME   = "gm." + StatisticsDAO.APPLICATION_VERSION_CODE_COLUMN_NAME;
    private static final String             UUID_FILTER_CONDITION_VAL_NAME          = "u."  + StatisticsDAO.USER_UUID_COLUMN_NAME;
    private static final String             HW_NUMBER_FILTER_CONDITION_VAL_NAME     = "gm." + StatisticsDAO.APPLICATION_HW_SERIAL_NUMBER_COLUMN_NAME;

    public  static final int                VALIDATE_DATA_VALUE                     = 1;
    public  static final int                VALIDATE_DATA_GAME_DURATION_MAX         = 60 * 1000 * 1000;

    public  static       List<String>       gameScoreKeys;


    @SuppressWarnings("unused")
    private enum CompleteDefinition implements INumberedDefinition {

        ANY         (FORM_ANY_VALUE,    "NULL",     "NOT NULL"),
        INCOMPLETE  (1,                 "NULL",     "NULL"),
        COMPLETE    (2,                 "NOT NULL", "NOT NULL");

        private int     number;
        private String  conditionValue1;
        private String  conditionValue2;

        CompleteDefinition(int number, String conditionValue1, String conditionValue2) {
            this.number             = number;
            this.conditionValue1    = conditionValue1;
            this.conditionValue2    = conditionValue2;
        }

        @Override
        public int number() {
            return number;
        }

        public String getConditionValue1() {
            return conditionValue1;
        }

        public String getConditionValue2() {
            return conditionValue2;
        }

        public static CompleteDefinition getCompleteDefinitionForCompleteNumber(int number) {
            for (CompleteDefinition completeGameDefinition : values()) {
                if (completeGameDefinition.number == number) {
                    return completeGameDefinition;
                }
            }
            return ANY;
        }
    }

    private final StatisticsDAO statisticsDAO;

    public StatisticsResource(StatisticsDAO statisticsDAO) {
        this.statisticsDAO = statisticsDAO;
    }

    @POST
    @Path("/general_info")
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    public Response generalInfo() {
        GeneralInfo generalInfo = statisticsDAO.getGeneralInfo();
        if (generalInfo != null) {
            return Response.ok(generalInfo).build();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Path("/last_game")
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    public Response lastGame() {
        LastGameInfo lastGameInfo = statisticsDAO.getLastGameInfo();
        if (lastGameInfo != null) {
            return Response.ok(lastGameInfo).build();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Path("/form_enums")
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    public Response formEnums() {

        Object result = new Object() {

            private List<FormPreparedValue> gameDefinitions = new ArrayList<FormPreparedValue>() {
                {
                    add(FORM_ANY_PREPARED_VALUE);
                    for (GameDefinition gameDefinition: GameDefinition.values()) {
                        add(new FormPreparedValue(gameDefinition.name(), gameDefinition.number()));
                    }
                }
            };

            private List<FormPreparedValue> difficultyDefinitions = new ArrayList<FormPreparedValue>() {
                {
                    for (DifficultyDefinition gameDifficulty: DifficultyDefinition.values()) {
                        add(new FormPreparedValue(gameDifficulty.name(), gameDifficulty.number()));
                    }
                }
            };

            private List<FormPreparedValue> localeDefinitions = new ArrayList<FormPreparedValue>() {
                {
                    add(FORM_ANY_PREPARED_VALUE);
                    for (LocaleDefinition localeDefinition: LocaleDefinition.values()) {
                        add(new FormPreparedValue(localeDefinition.name(), localeDefinition.number()));
                    }
                }
            };

            private List<FormPreparedValue> genderDefinitions = new ArrayList<FormPreparedValue>() {
                {
                    add(FORM_ANY_PREPARED_VALUE);
                    for (GenderDefinition genderDefinition: GenderDefinition.values()) {
                        add(new FormPreparedValue(genderDefinition.name(), genderDefinition.number()));
                    }
                }
            };

            private List<FormPreparedValue> completeDefinition = new ArrayList<FormPreparedValue>() {
                {
                    for (CompleteDefinition completeDefinition: CompleteDefinition.values()) {
                        add(new FormPreparedValue(completeDefinition.name(), completeDefinition.number()));
                    }
                }
            };

            private List<FormPreparedValue> gameVersions = statisticsDAO.getGameVersions();
            {
                gameVersions.add(0, FORM_ANY_PREPARED_VALUE); //Add ANY Game Version Entry
            }

            @SuppressWarnings("unused")
            public List<FormPreparedValue> getGameDefinitions() {
                return gameDefinitions;
            }

            @SuppressWarnings("unused")
            public List<FormPreparedValue> getDifficultyDefinitions() {
                return difficultyDefinitions;
            }

            @SuppressWarnings("unused")
            public List<FormPreparedValue> getLocaleDefinitions() {
                return localeDefinitions;
            }

            @SuppressWarnings("unused")
            public List<FormPreparedValue> getGenderDefinitions() {
                return genderDefinitions;
            }

            @SuppressWarnings("unused")
            public List<FormPreparedValue> getCompleteDefinition() {
                return completeDefinition;
            }

            @SuppressWarnings("unused")
            public List<FormPreparedValue> getGameVersions() {
                return gameVersions;
            }
        };

        return Response.ok(result).build();
    }

    @POST
    @Path("/export_csv")
    @Timed
    @Produces("text/csv; charset=utf-8")
    public Response exportSCV(FormValues formValues) {
        int[]    localeNumbers = formValues.getLocaleNumber();
        int[]    gameNumbers   = formValues.getGameNumber();
        int      genderNumber  = formValues.getGenderNumber();
        Long[]   versionCodes  = formValues.getApplicationVersionCode();
        String[] userUuids     = formValues.getUserId();
        String[] hwNumbers     = formValues.getHwSerialNumber();
        boolean validateData   = formValues.isValidateGames();

        CompleteDefinition  completeDefinition  = CompleteDefinition.getCompleteDefinitionForCompleteNumber(formValues.getCompleteNumber());
        GenderDefinition    genderDefinition    = GenderDefinition.getGenderDefinitionForGenderNumber(genderNumber);

        String listOfGameNumbers = prepareGamesFilter(gameNumbers);
        String listOfLocaleNumbers = prepareLocaleFilter(localeNumbers);

        String condition = String.format(StatisticsDAO.GAMES_EXPORT_CONDITION,
                formValues.getMinStartTime(),
                formValues.getMaxStartTime(),
                formValues.getMinAge(),
                formValues.getMaxAge(),
                listOfGameNumbers,
                formValues.getMinDifficultyNumber(),
                formValues.getMaxDifficultyNumber(),
                genderDefinition == null ? getAllNamesInString(GenderDefinition.values()) : String.format("\'%s\'", genderDefinition.name()),
                completeDefinition.getConditionValue1(), completeDefinition.getConditionValue2(),
                listOfLocaleNumbers,
                prepareSqlInFilter(APP_VERSION_FILTER_CONDITION_VAL_NAME, false, Integer.toString(FORM_ANY_VALUE), versionCodes),
                prepareSqlInFilter(UUID_FILTER_CONDITION_VAL_NAME,        true,  null,                             userUuids),
                prepareSqlInFilter(HW_NUMBER_FILTER_CONDITION_VAL_NAME,   true,  null,                             hwNumbers)
        );

        gameScoreKeys = statisticsDAO.getGameScoreKeys();

        //Create column names record
        StringBuilder result = new StringBuilder();
        result.append(GamesExportMapper.getColumnNamesRecord(gameScoreKeys));

        //Prepare all score parts of actual sql statement
        StringBuilder gameScoreSelect = new StringBuilder();
        StringBuilder gameScoreJoin   = new StringBuilder();
        StringBuilder gameScoreGroup  = new StringBuilder();

        prepareGameScoreParts(gameScoreKeys, gameScoreSelect, gameScoreJoin, gameScoreGroup);

        //Execute sql query and append the result into a StringBuilder...
        List<String> statisticsGames = statisticsDAO.getGamesForExport(gameScoreSelect.toString(), condition, gameScoreJoin.toString(), gameScoreGroup.toString(), validateData ? VALIDATE_DATA_VALUE : 0);
        for (String csvLine : statisticsGames) {
            result.append(csvLine).append("\n");
        }

            String filename = String.format(  EXPORTED_FILE_NAME_FORMAT,
                DateTimeFormat.forPattern("YYYYMMddHHmmss").print(new DateTime()));
        
        return Response.ok(result.toString())
                .header(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE)
                .header(HEADER_CONTENT_DISPOSITION_LABEL, String.format(HEADER_ATTACHMENT_FORMAT, filename)).build();
    }

    private String prepareGamesFilter(int[] gamesNumbers) {
        StringBuilder gamesList = new StringBuilder();

        for(int game : gamesNumbers) {
            if(game == FORM_ANY_VALUE) return getAllNumbersInString(GameDefinition.values());

            //TODO - Log an error if gameDefinition for the game number doesn't exist
            if(GameDefinition.getGameDefinitionForId(game) != null) {
                if(gamesList.length() != 0) gamesList.append(",");

                gamesList.append(game);
            }
        }

        if(gamesList.length() == 0) return getAllNumbersInString(GameDefinition.values());

        return gamesList.toString();
    }

    private String prepareLocaleFilter(int[] localeNumbers) {
        StringBuilder localeList = new StringBuilder();

        for(int locale : localeNumbers) {
            if(locale == FORM_ANY_VALUE) return getAllNumbersInString(LocaleDefinition.values()) + LOCALE_DEFINITION_LEGACY_KEYS;

            //TODO - Log an error if localeDefinition for the local number doesn't exist
            if(LocaleDefinition.getLocaleDefinitionForLocaleNumber(locale) != null) {
                if(localeList.length() != 0) localeList.append(",");

                localeList.append(locale);
            }
        }

        if(localeList.length() == 0) return getAllNumbersInString(LocaleDefinition.values()) + LOCALE_DEFINITION_LEGACY_KEYS;

        return localeList.toString();
    }

    /**
     * @param valueName Name of the value that's going to be compared to all values in SQL IN statement. example: "{valueName} IN (...);"
     * @param wrapValuesInApostrophes "IN ('val1', 'val2'...)" vs "IN (val1, val2...)"
     * @param anyValue if anything from values[] equals this, then returns empty String and this filter is omitted.
     * @return Empty String if values[] is null or empty. Otherwise returns formatted String according to SQL_IN_FILTER_CONDITION_FORMAT and parameters.
     */
    private <T> String prepareSqlInFilter(String valueName, boolean wrapValuesInApostrophes, String anyValue, T ... values) {
        if(values == null || values.length == 0) return new String();

        String valuesFormat = wrapValuesInApostrophes ? SQL_IN_FILTER_VALUE_FORMAT_APOSTROPHE : SQL_IN_FILTER_VALUE_FORMAT;
        StringBuilder userFilterBuilder = new StringBuilder();

        for(Object actualValue : values) {
            String actualValueString = actualValue.toString();

            if(anyValue != null && anyValue.equals(actualValueString)) return new String(); //Any value found!
            if(userFilterBuilder.length() != 0) userFilterBuilder.append(", ");

            userFilterBuilder.append(String.format(valuesFormat, actualValueString));
        }

        return String.format(SQL_IN_FILTER_CONDITION_FORMAT, valueName, userFilterBuilder.toString());
    }

    private void prepareGameScoreParts(List<String> gameScoreKeys, StringBuilder gameScoreSelect, StringBuilder gameScoreJoin, StringBuilder gameScoreGroup) {
        int iterator = 0;
        for(String gameScoreKey : gameScoreKeys) {
            String gsName = "gs" + iterator;

            gameScoreSelect.append(
                    gsName + ".value as \"" + gameScoreKey + "\", "
            );

            gameScoreJoin.append(
                    "LEFT JOIN game_score " + gsName + " " +
                            "ON " + gsName + ".key = '" + gameScoreKey + "' AND " + gsName + ".game_id = gm.id "
            );

            gameScoreGroup.append(
                    gsName + ".value, "
            );

            iterator++;
        }
    }

    private <T extends INumberedDefinition> String getAllNumbersInString(T[] values) {
        StringBuilder result = new StringBuilder();
        for (T numberedDefinition: values) {
            if (result.length() > 0) result.append(",");
            result.append(numberedDefinition.number());
        }
        return result.toString();
    }

    private <T extends INumberedDefinition> String getAllNamesInString(T[] values) {
        StringBuilder result = new StringBuilder();
        for (T numberedDefinition: values) {
            if (result.length() > 0) result.append(",");
            result.append(String.format("\'%s\'", numberedDefinition.name()));
        }
        return result.toString();
    }
}