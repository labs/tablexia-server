package cz.nic.tablexia.server.resources;

import com.codahale.metrics.annotation.Timed;
import cz.nic.tablexia.server.TablexiaServerBuildConfig;
import cz.nic.tablexia.shared.rest.VersionRestPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
@Path(VersionRestPath.VERSION_PATH)
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VersionResource {

    private static final String VERSION_LABEL= "Server version: %s\nModel version: %s (%d)";

    @GET
    @Path(VersionRestPath.VERSION_GET)
    @Timed
    public Response get() {
        return Response.ok(String.format(   VERSION_LABEL,
                                            TablexiaServerBuildConfig.SERVER_VERSION_NAME,
                                            TablexiaServerBuildConfig.MODEL_VERSION_NAME,
                                            TablexiaServerBuildConfig.MODEL_VERSION_CODE)).build();
    }
}
