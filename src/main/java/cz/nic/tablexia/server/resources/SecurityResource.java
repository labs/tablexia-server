package cz.nic.tablexia.server.resources;

import com.codahale.metrics.annotation.Timed;
import cz.nic.tablexia.shared.security.SecurityRestPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
@Path(SecurityRestPath.SECURITY_PATH)
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SecurityResource {

    @POST
    @Path(SecurityRestPath.SECURITY_GET)
    @Timed
    public Response getTime() {
        return Response.ok(getCurrentTime()).build();
    }

    // protected because we need to use it in test
    protected long getCurrentTime() {
        return System.currentTimeMillis();
    }
}
