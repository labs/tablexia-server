package cz.nic.tablexia.server.auth;

import java.util.Objects;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class Digest {

    private final String hash;
    private final long time;

    public Digest(String hash, long time) {
        this.hash = checkNotNull(hash);
        this.time = time;
    }

    public String getHash() {
        return hash;
    }

    public long getTime() {
        return time;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hash);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Digest other = (Digest) obj;
        return Objects.equals(this.hash, other.hash);
    }
}
