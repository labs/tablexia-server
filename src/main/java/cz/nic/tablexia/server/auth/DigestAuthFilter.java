package cz.nic.tablexia.server.auth;

import cz.nic.tablexia.shared.security.SecurityRestHeader;
import io.dropwizard.auth.AuthFilter;
import io.dropwizard.auth.AuthenticationException;
import java.io.IOException;
import java.security.Principal;
import java.util.Optional;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class DigestAuthFilter<P extends Principal> extends AuthFilter<Digest, P> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DigestAuthFilter.class);

    public DigestAuthFilter() {

    }

    @Override
    public void filter(final ContainerRequestContext crc) throws IOException {
        String key = crc.getHeaderString(SecurityRestHeader.HEADER_AUTH_KEY);
        String timeString = crc.getHeaderString(SecurityRestHeader.HEADER_AUTH_TIME);
        if (key == null || key.isEmpty() || timeString == null || timeString.isEmpty()) {
            throw new WebApplicationException(unauthorizedHandler.buildResponse(prefix, realm));
        }

        long time = Long.parseLong(timeString);
        final Digest credentials = new Digest(key, time);
        try {
            final Optional<P> principal = authenticator.authenticate(credentials);

            if (principal.isPresent()) {
                crc.setSecurityContext(new SecurityContext() {
                    @Override
                    public Principal getUserPrincipal() {
                        return principal.get();
                    }

                    @Override
                    public boolean isUserInRole(String role) {
                        return authorizer.authorize(principal.get(), role);
                    }

                    @Override
                    public boolean isSecure() {
                        return crc.getSecurityContext().isSecure();
                    }

                    @Override
                    public String getAuthenticationScheme() {
                        return SecurityContext.DIGEST_AUTH;
                    }
                });

                return;
            }


        } catch (AuthenticationException ex) {
            LOGGER.warn("Error authenticating credentials", ex);
            throw new InternalServerErrorException();
        }

        throw new WebApplicationException(unauthorizedHandler.buildResponse(prefix, realm));
    }

    public static class Builder<P extends Principal> extends
            AuthFilterBuilder<Digest, P, DigestAuthFilter<P>> {

        @Override
        protected DigestAuthFilter<P> newInstance() {
            return new DigestAuthFilter<>();
        }
    }
}
