package cz.nic.tablexia.server.auth;

import java.security.Principal;
import java.util.Objects;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class Authorized implements Principal {

    private final String name = "autorized";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Authorized other = (Authorized) obj;
        return Objects.equals(this.name, other.name);
    }
}
