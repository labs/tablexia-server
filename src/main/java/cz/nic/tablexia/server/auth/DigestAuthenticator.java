package cz.nic.tablexia.server.auth;

import cz.nic.tablexia.shared.security.SecurityHelper;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class DigestAuthenticator implements Authenticator<Digest, Authorized> {

    private static final Logger LOG = LoggerFactory.getLogger(DigestAuthenticator.class);
    private final String secretKey;
    private final int timeTolerance;

    public DigestAuthenticator(String secretKey, int timeTolerance) {
        this.secretKey = secretKey;
        this.timeTolerance = timeTolerance;
    }

    @Override
    public Optional<Authorized> authenticate(Digest credentials) throws AuthenticationException {
        String secret;
        try {
            secret = SecurityHelper.getSecretHash(secretKey, credentials.getTime());
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            LOG.error("Failed to create secret key", ex);
            return Optional.empty();
        }

        if (credentials.getHash().equals(secret) && checkTime(credentials)) {
            return Optional.of(new Authorized());
        }

        return Optional.empty();
    }

    private boolean checkTime(Digest credentials) {
       long currentTimeMillis = System.currentTimeMillis();

        Calendar remoteTime = Calendar.getInstance();
        remoteTime.setTime(new Date(credentials.getTime()));

        Calendar beforeTime = Calendar.getInstance();
        beforeTime.setTime(new Date(currentTimeMillis));
        beforeTime.add(Calendar.MINUTE, -timeTolerance);

        Calendar afterTime = Calendar.getInstance();
        afterTime.setTime(new Date(currentTimeMillis));
        afterTime.add(Calendar.MINUTE, timeTolerance);

        return beforeTime.before(remoteTime) && afterTime.after(remoteTime);
    }
}
