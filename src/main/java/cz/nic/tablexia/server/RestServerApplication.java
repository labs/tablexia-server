package cz.nic.tablexia.server;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;

import cz.nic.tablexia.server.auth.Authorized;
import cz.nic.tablexia.server.auth.DigestAuthFilter;
import cz.nic.tablexia.server.auth.DigestAuthenticator;
import cz.nic.tablexia.server.dao.CustomAvatarDAO;
import cz.nic.tablexia.server.dao.StatisticsDAO;
import cz.nic.tablexia.server.dao.UserDAO;
import cz.nic.tablexia.server.resources.SecurityResource;
import cz.nic.tablexia.server.resources.StatisticsResource;
import cz.nic.tablexia.server.resources.UserResource;
import cz.nic.tablexia.server.resources.VersionResource;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import java.util.logging.Logger;
import org.glassfish.jersey.filter.LoggingFilter;
import org.skife.jdbi.v2.DBI;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class RestServerApplication extends Application<RestServerConfiguration> {

    public static void main(String[] args) throws Exception {
        new RestServerApplication().run(args);
    }

    @Override
    public String getName() {
        return "Tablexia REST server";
    }

    @Override
    public void initialize(Bootstrap<RestServerConfiguration> bootstrap) {
        bootstrap.addBundle(new MigrationsBundle<RestServerConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(RestServerConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
    }

    @Override
    public void run(RestServerConfiguration configuration, Environment environment) {

        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "postgresql");
        final UserDAO userDao = jdbi.onDemand(UserDAO.class);
        final CustomAvatarDAO avatarDao = jdbi.onDemand(CustomAvatarDAO.class);
        final StatisticsDAO statisticsDao = jdbi.onDemand(StatisticsDAO.class);

        environment.jersey().register(new AuthDynamicFeature(new DigestAuthFilter.Builder<Authorized>()
                .setAuthenticator(new DigestAuthenticator(configuration.getSecretKey(), configuration.getSecretTimeTolerance()))
                .setRealm(getName())
                .buildAuthFilter()));

        //application
        environment.jersey().register(new UserResource(userDao, avatarDao));
        environment.jersey().register(new SecurityResource());
        environment.jersey().register(new VersionResource());

        //statistics
        environment.jersey().register(new StatisticsResource(statisticsDao));

        // Jackson configuration
        environment.getObjectMapper().enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT);
        environment.getObjectMapper().enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        environment.getObjectMapper().enable(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY);
        environment.getObjectMapper().enable(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER);
        environment.getObjectMapper().enable(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS);

        // Default byte array serializer uses Base64!
        SimpleModule module = new SimpleModule();
        module.addSerializer(byte[].class, new ByteArraySerializer());
        environment.getObjectMapper().registerModule(module);

        // do your stuff and then add LoggingFilter
        environment.jersey().register(new LoggingFilter(Logger.getLogger(LoggingFilter.class.getName()), true));
    }

}