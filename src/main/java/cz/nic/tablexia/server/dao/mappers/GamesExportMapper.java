package cz.nic.tablexia.server.dao.mappers;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.server.dao.StatisticsDAO;
import cz.nic.tablexia.server.model.statistics.ServerOptimizedGame;
import cz.nic.tablexia.server.resources.StatisticsResource;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameDefinition;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;
import cz.nic.tablexia.shared.model.definitions.LocaleDefinition;

public class GamesExportMapper implements ResultSetMapper<String> {
    private static final String COLUMN_NAME_BASE_RECORD  = "game_id;start_time;date;time;game_duration;game;difficulty;user_uuid;user;user_gender;user_current_age;user_age;user_rank;user_avatar;locale;app_version;hw_serial";
    private static final String COLUMN_NAME_RESULT       = ";game_result";

    private static final String COLUMN_NAME_SCORE_FORMAT = ";%s"; //Score column names are dynamically appended after column name base record above

    private static final String USER_NAME_STRING_FORMAT  = "\"%s\"";

    private static final String CSV_RECORD_FORMAT   = "%d;%d;%s;%s;%d;%s;%s;%s;%s;%s;%d;%d;%d;%d;%s;%s;%s";
    private static final String CSV_RESULT_FORMAT   = ";%s";
    private static final String CSV_SCORE_FORMAT    = ";%s"; //CSV Score records are dynamically appended after csv record above
    private static final String BASE_UNKNOWN_VALUE  = "UNKNOWN";
    private static final String SCORE_UNKNOWN_VALUE = "";
    private static final String DATE_FORMAT         = "dd.MM.yyyy";
    private static final String TIME_FORMAT         = "HH:mm";

    //Stores game scores for a game that is currently being iterated
    private static final List<GameScore> tempGameScores = new ArrayList<GameScore>(5);

    @Override
    public String map(int i, ResultSet rs, StatementContext sc) throws SQLException {
        tempGameScores.clear();

        long start_time = rs.getLong(StatisticsDAO.GAME_START_TIME_COLUMN_NAME);
        long game_duration = rs.getLong(StatisticsDAO.GAME_DURATION_COLUMN_NAME);
        DateTime dateTime = new DateTime(start_time);
        GameDefinition gameDefinition = GameDefinition.getGameDefinitionForId(rs.getInt(StatisticsDAO.GAME_NUMBER_COLUMN_NAME));
        DifficultyDefinition difficultyDefinition = DifficultyDefinition.getDifficultyDefinitionForDifficultyNumber(rs.getInt(StatisticsDAO.GAME_DIFFICULTY_COLUMN_NAME));
        LocaleDefinition localeDefinition = LocaleDefinition.getLocaleDefinitionForLocaleNumber(rs.getInt(StatisticsDAO.APPLICATION_LOCALE_COLUMN_NAME));

        String baseRecord = String.format(CSV_RECORD_FORMAT,
                rs.getLong(StatisticsDAO.GAME_ID_COLUMN_NAME),
                start_time,
                DateTimeFormat.forPattern(DATE_FORMAT).print(dateTime),
                DateTimeFormat.forPattern(TIME_FORMAT).print(dateTime),
                game_duration,
                gameDefinition != null ? gameDefinition : BASE_UNKNOWN_VALUE,
                difficultyDefinition != null ? difficultyDefinition : BASE_UNKNOWN_VALUE,
                rs.getString(StatisticsDAO.USER_UUID_COLUMN_NAME),
                String.format(USER_NAME_STRING_FORMAT, rs.getString(StatisticsDAO.USER_NAME_COLUMN_NAME)),
                rs.getString(StatisticsDAO.USER_GENDER_COLUMN_NAME),
                rs.getInt(StatisticsDAO.USER_AGE_COLUMN_NAME),
                rs.getInt(StatisticsDAO.GAME_USER_AGE_COLUMN_NAME),
                rs.getInt(StatisticsDAO.GAME_USER_RANK_COLUMN_NAME),
                rs.getInt(StatisticsDAO.USER_AVATAR_COLUMN_NAME),
                localeDefinition != null ? localeDefinition : BASE_UNKNOWN_VALUE,
                rs.getString(StatisticsDAO.APPLICATION_VERSION_NAME_COLUMN_NAME),
                rs.getString(StatisticsDAO.APPLICATION_HW_SERIAL_NUMBER_COLUMN_NAME));

        StringBuilder gameScoreRecord = new StringBuilder();
        for(String score : StatisticsResource.gameScoreKeys) {
            String actualScore = rs.getString(score);
            gameScoreRecord.append(String.format(CSV_SCORE_FORMAT, actualScore == null ? SCORE_UNKNOWN_VALUE: actualScore));

            if(actualScore != null) tempGameScores.add(new GameScore(score, actualScore));
        }

        GameResultDefinition result = getGameResult(tempGameScores, gameDefinition, difficultyDefinition, rs.getInt(StatisticsDAO.GAME_SCORE_RESOLVER_VERSION), game_duration);
        String resultRecord = String.format(CSV_RESULT_FORMAT, Integer.toString(result.number()));

        return baseRecord.toString() + resultRecord + gameScoreRecord.toString();
    }

    private GameResultDefinition getGameResult(List<GameScore> gameScores, GameDefinition gameDefinition, DifficultyDefinition difficultyDefinition, Integer gameScoreResolverVersion, Long gameDuration) {
        GameResultDefinition result = GameResultDefinition.NO_STAR;

        if(gameDuration > 0 && difficultyDefinition != DifficultyDefinition.TUTORIAL) {
            ServerOptimizedGame game = new ServerOptimizedGame(gameDefinition, difficultyDefinition, gameDuration);
            game.setGameScoreMap(gameScores);

            result = gameDefinition.getGameScoreResolver(gameScoreResolverVersion).getGameCupsResult(game);
        }

        return result;
    }

    public static String getColumnNamesRecord(List<String> scoreKeys) {
        StringBuilder result = new StringBuilder();

        result.append(COLUMN_NAME_BASE_RECORD);
        result.append(COLUMN_NAME_RESULT);

        for(String score : scoreKeys) {
            result.append(String.format(COLUMN_NAME_SCORE_FORMAT, score.toLowerCase()));
        }
        result.append("\n");

        return result.toString();
    }
}
