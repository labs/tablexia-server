package cz.nic.tablexia.server.dao.mappers;

import cz.nic.tablexia.shared.model.GameScore;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class GameScoreMapper implements ResultSetMapper<GameScore> {

    @Override
    public GameScore map(int i, ResultSet rs, StatementContext sc) throws SQLException {
        return new GameScore(rs.getString("key"), rs.getString("value"));
    }
}
