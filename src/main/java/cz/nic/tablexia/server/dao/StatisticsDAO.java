package cz.nic.tablexia.server.dao;

import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import java.util.List;

import cz.nic.tablexia.server.dao.mappers.GameScoreKeyMapper;
import cz.nic.tablexia.server.dao.mappers.GamesExportMapper;
import cz.nic.tablexia.server.model.statistics.FormPreparedValue;
import cz.nic.tablexia.server.model.statistics.GeneralInfo;
import cz.nic.tablexia.server.model.statistics.LastGameInfo;
import cz.nic.tablexia.server.resources.StatisticsResource;
import cz.nic.tablexia.shared.model.Game;

@UseStringTemplate3StatementLocator
public abstract class StatisticsDAO implements Transactional<StatisticsDAO> {

    public static final String GAME_NUMBER_COLUMN_NAME                  = "game_number";
    public static final String USER_UUID_COLUMN_NAME                    = "uuid";
    public static final String USER_NAME_COLUMN_NAME                    = "user_name";
    public static final String APPLICATION_VERSION_NAME_COLUMN_NAME     = "application_version_name";

    public static final String PLAYED_GAMES_COUNT_COLUMN_NAME           = "played_games_count";
    public static final String COMPLETE_GAMES_COUNT_COLUMN_NAME         = "complete_games_count";
    public static final String USERS_COUNT_COLUMN_NAME                  = "users_count";

    public static final String START_TIME_COLUMN_NAME                   = "start_time";
    public static final String APPLICATION_VERSION_CODE_COLUMN_NAME     = "application_version_code";
    public static final String GAME_SCORE_RESOLVER_VERSION              = "game_score_resolver_version";

    public static final String GAME_ID_COLUMN_NAME                      = "game_id";
    public static final String GAME_START_TIME_COLUMN_NAME              = "game_start_time";
    public static final String GAME_DURATION_COLUMN_NAME                = "game_duration";
    public static final String GAME_DIFFICULTY_COLUMN_NAME              = "game_difficulty";
    public static final String GAME_USER_RANK_COLUMN_NAME               = "game_user_rank";
    public static final String GAME_USER_AGE_COLUMN_NAME                = "game_user_age";
    public static final String USER_GENDER_COLUMN_NAME                  = "user_gender";
    public static final String USER_AVATAR_COLUMN_NAME                  = "user_avatar";
    public static final String USER_AGE_COLUMN_NAME                     = "user_age";
    public static final String APPLICATION_LOCALE_COLUMN_NAME           = "application_locale";
    public static final String APPLICATION_HW_SERIAL_NUMBER_COLUMN_NAME = "hw_serial_number";

    @Mapper(GeneralInfo.GeneralInfoMapper.class)
    @SqlQuery("SELECT " +
            "   (SELECT count(id) " +
            "    FROM   game) as " + PLAYED_GAMES_COUNT_COLUMN_NAME + ", " +
            "   (SELECT count(id) " +
            "    FROM   game " +
            "    WHERE  end_time IS NOT NULL AND end_time != 0) as " + COMPLETE_GAMES_COUNT_COLUMN_NAME + ", " +
            "   (SELECT count(id) " +
            "    FROM \"user\") as " + USERS_COUNT_COLUMN_NAME)
    public abstract GeneralInfo getGeneralInfo();

    @Mapper(LastGameInfo.LastGameMapper.class)
    @SqlQuery("SELECT   g.game_number as " + GAME_NUMBER_COLUMN_NAME + ", " +
            "           g.start_time as " + START_TIME_COLUMN_NAME + ", " +
            "           u.name as " + USER_NAME_COLUMN_NAME +
            "  FROM game g " +
            "       JOIN \"user\" u ON g.user_id = u.id " +
            "  WHERE" +
            "       g.start_time IS NOT NULL" +
            "  ORDER BY g.id DESC " +
            "  LIMIT 1")
    public abstract LastGameInfo getLastGameInfo();

    @Mapper(FormPreparedValue.ApplicationVersionMapper.class)
    @SqlQuery("SELECT DISTINCT ON (application_version_name) " +
            "       application_version_name as " + APPLICATION_VERSION_NAME_COLUMN_NAME + ", " +
            "       application_version_code as " + APPLICATION_VERSION_CODE_COLUMN_NAME +
            "  FROM game")
    public abstract List<FormPreparedValue> getGameVersions();

    @Mapper(GameScoreKeyMapper.class)
    @SqlQuery("SELECT DISTINCT key FROM game_score")
    public abstract List<String> getGameScoreKeys();


    public static final String GAMES_EXPORT_CONDITION =
            "gm.start_time >= %d AND " +
            "gm.start_time <= %d AND " +
            //User age is in range or DEFAULT VALUE
            "((gm.user_age >= %d AND gm.user_age <= %d) OR gm.user_age = " + Game.DEFAULT_USER_AGE + ") AND " +
            "gm.game_number IN (%s) AND " +
            "gm.difficulty_number >= %d AND " +
            "gm.difficulty_number <= %d AND " +
            "u.gender IN (%s) AND " +
            "(gm.end_time IS %s OR gm.end_time IS %s) AND " +
            "gm.locale IN (%s) " +
            "%s " + //Application version condition
            "%s " + //User UUID condition
            "%s";   //Hardware number condition

    private static final String GAME_DURATION = "greatest(0, ((gm.end_time - gm.start_time) - SUM(COALESCE(gp.end_time, gp.start_time, 0) - COALESCE(gp.start_time, 0))))";

    @Mapper(GamesExportMapper.class)
    @SqlQuery("SELECT " +
            "     <gameScoreSelect> " +
            "     gm.id as " + GAME_ID_COLUMN_NAME + "," +
            "     gm.start_time as " + GAME_START_TIME_COLUMN_NAME + "," +
            "     " + GAME_DURATION + " as " + GAME_DURATION_COLUMN_NAME + "," +
            "     gm.application_version_code as " + APPLICATION_VERSION_CODE_COLUMN_NAME + "," +
            "     gm.game_number as " + GAME_NUMBER_COLUMN_NAME + "," +
            "     gm.difficulty_number as " + GAME_DIFFICULTY_COLUMN_NAME + "," +
            "     u.name as " + USER_NAME_COLUMN_NAME + "," +
            "     u.uuid as " + USER_UUID_COLUMN_NAME + "," +
            "     u.gender as " + USER_GENDER_COLUMN_NAME + "," +
            "     u.age as " + USER_AGE_COLUMN_NAME + "," +
            "     gm.user_age as " + GAME_USER_AGE_COLUMN_NAME + "," +
            "     gm.user_rank as " + GAME_USER_RANK_COLUMN_NAME + "," +
            "     u.avatar as " + USER_AVATAR_COLUMN_NAME + "," +
            "     gm.locale as " + APPLICATION_LOCALE_COLUMN_NAME + "," +
            "     gm.application_version_name as " + APPLICATION_VERSION_NAME_COLUMN_NAME + "," +
            "     gm.hw_serial_number as " + APPLICATION_HW_SERIAL_NUMBER_COLUMN_NAME + "," +
            "     gm.game_score_resolver_version as " + GAME_SCORE_RESOLVER_VERSION + " " +
            "FROM game gm " +
            "<gameScoreJoin>" +
            "LEFT JOIN game_pause gp" +
            "     ON gp.game_id = gm.id " +
            "JOIN \"user\" u" +
            "     ON gm.user_id = u.id " +
            "WHERE" +
            "     <condition> " +
            "GROUP BY" +
            "     <gameScoreGroup>" +
            "     gm.id," +
            "     gm.start_time," +
            "     gm.end_time," +
            "     u.uuid," +
            "     u.name," +
            "     u.gender," +
            "     u.age," +
            "     gm.user_age," +
            "     u.avatar " +
            "HAVING" +
            "     " + StatisticsResource.VALIDATE_DATA_VALUE + " != <validateData> OR (" +
            "     " + GAME_DURATION + " >= 0 AND" +
            "     " + StatisticsResource.VALIDATE_DATA_GAME_DURATION_MAX + " > " + GAME_DURATION + ")"
            )
    public abstract List<String> getGamesForExport(
            @Define("gameScoreSelect") String gameScoresSelect,
            @Define("condition") String condition,
            @Define("gameScoreJoin") String gameScoreJoin,
            @Define("gameScoreGroup") String gameScoreGroup,
            @Define("validateData") int validateData
    );

    public abstract void close();
}