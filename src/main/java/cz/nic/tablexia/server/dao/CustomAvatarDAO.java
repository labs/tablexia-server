package cz.nic.tablexia.server.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import cz.nic.tablexia.server.dao.mappers.CustomAvatarDataPacketMapper;
import cz.nic.tablexia.shared.model.CustomAvatarDataPacket;

/**
 * Created by drahomir on 1/11/17.
 */

public abstract class CustomAvatarDAO implements Transactional<CustomAvatarDAO> {
    public static final String AVATAR_DATA_ALIAS_NAME = "data";
    public static final String SYNC_AT_ALIAS_NAME     = "sync_at";

    @Mapper(CustomAvatarDataPacketMapper.class)
    @SqlQuery("select data as " + AVATAR_DATA_ALIAS_NAME + ", sync_at as " + SYNC_AT_ALIAS_NAME + " from custom_avatar where user_id = :user_id")
    public abstract CustomAvatarDataPacket getCustomUserAvatar(@Bind("user_id") long user_id);

    @SqlUpdate("delete from custom_avatar WHERE user_id = :user_id")
    public abstract void deleteCustomUserAvatar(@Bind("user_id") long user_id);

    @SqlUpdate("insert into custom_avatar(user_id, data, sync_at) values (:user_id, :avatar_data, :sync_at)")
    public abstract void insertCustomUserAvatar(@Bind("user_id") long user_id, @Bind("avatar_data") byte[] avatar_data, @Bind("sync_at") long syncAt);

    /**
     * close with no args is used to close the connection
     */
    public abstract void close();
}
