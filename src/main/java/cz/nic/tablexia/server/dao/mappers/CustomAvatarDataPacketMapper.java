package cz.nic.tablexia.server.dao.mappers;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;

import cz.nic.tablexia.server.dao.CustomAvatarDAO;
import cz.nic.tablexia.shared.model.CustomAvatarDataPacket;

/**
 * Created by drahomir on 1/6/17.
 */

public class CustomAvatarDataPacketMapper implements ResultSetMapper<CustomAvatarDataPacket> {
    @Override
    public CustomAvatarDataPacket map(int index, ResultSet rs, StatementContext ctx) throws SQLException {
        byte[] avatarData = rs.getBytes(CustomAvatarDAO.AVATAR_DATA_ALIAS_NAME);
        if(avatarData == null || avatarData.length == 0) return null;
        long syncAt = rs.getLong(CustomAvatarDAO.SYNC_AT_ALIAS_NAME);
        return new CustomAvatarDataPacket(avatarData, syncAt);
    }
}