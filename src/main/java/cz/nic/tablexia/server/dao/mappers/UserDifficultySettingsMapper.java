package cz.nic.tablexia.server.dao.mappers;

import cz.nic.tablexia.shared.model.UserDifficultySettings;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class UserDifficultySettingsMapper implements ResultSetMapper<UserDifficultySettings> {

    @Override
    public UserDifficultySettings map(int i, ResultSet rs, StatementContext sc) throws SQLException {
        return new UserDifficultySettings(rs.getLong("user_id"), rs.getInt("game_number"), rs.getInt("difficulty_number"));
    }

}
