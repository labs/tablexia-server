package cz.nic.tablexia.server.dao.mappers;

import cz.nic.tablexia.shared.model.Game;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class GameMapper implements ResultSetMapper<Game> {

    @Override
    public Game map(int i, ResultSet rs, StatementContext sc) throws SQLException {
        return new Game(rs.getLong("id"),
                        null,
                        rs.getInt("difficulty_number"),
                        rs.getInt("game_number"),
                        rs.getLong("random_seed"),
                        rs.getLong("start_time"),
                        rs.getLong("end_time"),
                        rs.getInt("locale"),
                        rs.getString("application_version_name"),
                        rs.getInt("application_version_code"),
                        rs.getString("model_version_name"),
                        rs.getInt("model_version_code"),
                        rs.getInt("build_type"),
                        rs.getInt("platform"),
                        rs.getString("hw_serial_number"),
                        rs.getInt("rank_system_version_code"),
                        rs.getInt("game_score_resolver_version"),
                        rs.getInt("user_rank"),
                        rs.getInt("user_age")
                );
    }
}
