package cz.nic.tablexia.server.dao.mappers;

import cz.nic.tablexia.shared.model.Screen;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class ScreenMapper implements ResultSetMapper<Screen> {

    @Override
    public Screen map(int i, ResultSet rs, StatementContext sc) throws SQLException {
        return new Screen(0, 0, rs.getString("screen_name"), rs.getLong("time"));
    }
}
