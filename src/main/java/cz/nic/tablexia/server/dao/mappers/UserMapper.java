package cz.nic.tablexia.server.dao.mappers;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.definitions.GenderDefinition;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class UserMapper implements ResultSetMapper<User> {

    @Override
    public User map(int i, ResultSet rs, StatementContext sc) throws SQLException {
        User user = new User(rs.getLong("id"), rs.getString("name"), rs.getInt("age")
                        , GenderDefinition.valueOf(rs.getString("gender")), rs.getString("avatar"), rs.getString("signature")
                        , rs.getBoolean("deleted"), rs.getBoolean("help"), rs.getBoolean("intro"));
        user.setUuid(rs.getString("uuid"));
        return user;
    }
}
