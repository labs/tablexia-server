package cz.nic.tablexia.server.dao.mappers;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by drahomir on 11/7/16.
 */
public class GameScoreKeyMapper implements ResultSetMapper<String> {
    @Override
    public String map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        return r.getString("key");
    }
}
