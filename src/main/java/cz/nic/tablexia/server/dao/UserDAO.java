package cz.nic.tablexia.server.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import java.util.List;

import cz.nic.tablexia.server.dao.mappers.GameMapper;
import cz.nic.tablexia.server.dao.mappers.GamePauseMapper;
import cz.nic.tablexia.server.dao.mappers.GameScoreMapper;
import cz.nic.tablexia.server.dao.mappers.ScreenMapper;
import cz.nic.tablexia.server.dao.mappers.UserDifficultySettingsMapper;
import cz.nic.tablexia.server.dao.mappers.UserMapper;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GamePause;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.Screen;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.UserDifficultySettings;
import cz.nic.tablexia.shared.model.definitions.GenderDefinition;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public abstract class UserDAO implements Transactional<UserDAO> {

    @SqlUpdate("insert into \"user\" (name, signature, age, gender, avatar, deleted, intro, help) values (:name, :signature, :age, :gender, :avatar, :deleted, :intro, :help)")
    @GetGeneratedKeys(columnName = "id")
    public abstract long insert(@Bind("name") String name
            , @Bind("signature") String signature
            , @Bind("age") int age
            , @Bind("gender") GenderDefinition gender
            , @Bind("avatar") String avatar
            , @Bind("deleted") boolean deleted
            , @Bind("intro") boolean intro
            , @Bind("help") boolean help);

    @SqlUpdate("update \"user\" set name = :name, signature = :signature, age = :age, gender = :gender, avatar = :avatar, deleted = :deleted, intro = :intro, help = :help where uuid = cast(:uuid as uuid)")
    public abstract void update(@Bind("uuid") String uuid
            , @Bind("name") String name
            , @Bind("signature") String signature
            , @Bind("age") int age
            , @Bind("gender") GenderDefinition gender
            , @Bind("avatar") String avatar
            , @Bind("deleted") boolean deleted
            , @Bind("intro") boolean intro
            , @Bind("help") boolean help);

    @SqlUpdate("update \"user\" set received = true where uuid = cast(:uuid as uuid)")
    public abstract void confirmUser(@Bind("uuid") String uuid);

    @SqlUpdate("update \"user\" set deleted = true where uuid = cast(:uuid as uuid)")
    public abstract void markUserDeleted(@Bind("uuid") String uuid);

    @Mapper(UserMapper.class)
    @SqlQuery("select * from \"user\" where uuid = cast(:uuid as uuid)")
    public abstract User findUserByUuid(@Bind("uuid") String uuid);

    @Mapper(UserMapper.class)
    @SqlQuery("select * from \"user\" where id = :id")
    public abstract User findById(@Bind("id") long id);

    @Mapper(GameMapper.class)
    @SqlQuery("select * from game where user_id = :user_id")
    public abstract List<Game> findGamesByUserId(@Bind("user_id") long userId);

    @Mapper(GamePauseMapper.class)
    @SqlQuery("select * from game_pause where game_id = :game_id")
    public abstract List<GamePause> findGamePausesByGameId(@Bind("game_id") long gameId);

    @Mapper(GameScoreMapper.class)
    @SqlQuery("select * from game_score where game_id = :game_id")
    public abstract List<GameScore> findGameScoresByGameId(@Bind("game_id") long gameId);

    @Mapper(ScreenMapper.class)
    @SqlQuery("select * from screen where user_id = :user_id")
    public abstract List<Screen> findScreensByUserId(@Bind("user_id") long userId);

    @SqlUpdate("insert into game (user_id, start_time, end_time, difficulty_number, game_number, random_seed) values (:user_id , :start_time, :end_time, :difficulty_number, :game_number, :random_seed)")
    @GetGeneratedKeys(columnName = "id")
    public abstract long insertGameOld(
              @Bind("user_id") long userId
            , @Bind("start_time") Long startTime
            , @Bind("end_time") Long endTime
            , @Bind("difficulty_number") Integer difficultyNumber
            , @Bind("game_number") Integer gameNumber
            , @Bind("random_seed") Long randomSeed);

    @SqlUpdate("insert into game (user_id, start_time, end_time, difficulty_number, game_number, random_seed, locale, application_version_name, application_version_code, model_version_name, model_version_code, build_type, platform, hw_serial_number, rank_system_version_code, game_score_resolver_version, user_rank, user_age) values (:user_id , :start_time, :end_time, :difficulty_number, :game_number, :random_seed, :locale, :application_version_name, :application_version_code, :model_version_name, :model_version_code, :build_type, :platform, :hw_serial_number, :rank_system_version_code, :game_score_resolver_version, :user_rank, :user_age)")
    @GetGeneratedKeys(columnName = "id")
    public abstract long insertGame(
              @Bind("user_id") long userId
            , @Bind("start_time") Long startTime
            , @Bind("end_time") Long endTime
            , @Bind("difficulty_number") Integer difficultyNumber
            , @Bind("game_number") Integer gameNumber
            , @Bind("random_seed") Long randomSeed
            , @Bind("locale") Integer locale
            , @Bind("application_version_name") String applicationVersionName
            , @Bind("application_version_code") Integer applicationVersionCode
            , @Bind("model_version_name") String modelVersionName
            , @Bind("model_version_code") Integer modelVersionCode
            , @Bind("build_type") Integer buildType
            , @Bind("platform") Integer platform
            , @Bind("hw_serial_number") String hwSerialNumber
            , @Bind("rank_system_version_code") Integer rankSystemVersionCode
            , @Bind("game_score_resolver_version") Integer gameScoreResultResolver
            , @Bind("user_rank") int userRank
            , @Bind("user_age") int userAge
    );

    @SqlUpdate("insert into game_score (game_id, key, value) values (:game_id, :key, :value)")
    @GetGeneratedKeys(columnName = "id")
    public abstract long insertGameScore(@Bind("game_id") Long gameId
            , @Bind("key") String key
            , @Bind("value") String value);

    @SqlUpdate("insert into game_pause (game_id, start_time, end_time) values (:game_id, :start_time, :end_time)")
    @GetGeneratedKeys(columnName = "id")
    public abstract long insertGamePause(@Bind("game_id") Long gameId
            , @Bind("start_time") Long key
            , @Bind("end_time") Long value);

    @SqlUpdate("insert into screen (user_id, screen_name, time) values (:user_id, :screen_name, :time)")
    @GetGeneratedKeys(columnName = "id")
    public abstract long insertScreenVisited(@Bind("user_id") long userId
            , @Bind("screen_name") String screenName
            , @Bind("time") Long time);

    public List<Game> getAllGamesWithPausesAndScores(long userId) {
        List<Game> games = findGamesByUserId(userId);
        for (Game game : games) {
            game.setGamePauses(findGamePausesByGameId(game.getId()));
            game.getGameScoreMap().addAll(findGameScoresByGameId(game.getId()));
        }
        return games;
    }

    @SqlUpdate("insert into user_difficulty_setting(user_id, game_number, difficulty_number) values (:user_id, :game_number, :difficulty_number)")
    public abstract void insertUserDifficultySetting(@Bind("user_id") long userId
            , @Bind("game_number") int gameNumber
            , @Bind("difficulty_number") int difficultyNumber);

    @SqlUpdate("delete from user_difficulty_setting where user_id = :user_id")
    public abstract void deleteUserDifficultySetting(@Bind("user_id") long userId);

    @Mapper(UserDifficultySettingsMapper.class)
    @SqlQuery("select * from user_difficulty_setting where user_id = :user_id")
    public abstract List<UserDifficultySettings> getAllUserDifficultySetting(@Bind("user_id") long user_id);

    /**
     * close with no args is used to close the connection
     */
    public abstract void close();
}