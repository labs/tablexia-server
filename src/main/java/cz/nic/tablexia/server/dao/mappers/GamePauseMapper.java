package cz.nic.tablexia.server.dao.mappers;

import cz.nic.tablexia.shared.model.GamePause;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class GamePauseMapper implements ResultSetMapper<GamePause> {

    @Override
    public GamePause map(int i, ResultSet rs, StatementContext sc) throws SQLException {
        return new GamePause(rs.getLong("id"), rs.getLong("start_time"), rs.getLong("end_time"));
    }
}
