package cz.nic.tablexia.server.model.statistics;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import cz.nic.tablexia.server.dao.StatisticsDAO;
import cz.nic.tablexia.shared.model.definitions.GameDefinition;

public class LastGameInfo {

    public static class LastGameMapper implements ResultSetMapper<LastGameInfo> {
        @Override
        public LastGameInfo map(int i, ResultSet rs, StatementContext sc) throws SQLException {
            return new LastGameInfo(    rs.getInt(StatisticsDAO.GAME_NUMBER_COLUMN_NAME),
                                        rs.getLong(StatisticsDAO.START_TIME_COLUMN_NAME),
                                        rs.getString(StatisticsDAO.USER_NAME_COLUMN_NAME));
        }
    }

    private GameDefinition  gameDefinition;
    private long            time;
    private String          userName;

    public LastGameInfo(int gameNumber, long time, String userName) {
        this.gameDefinition = GameDefinition.getGameDefinitionForId(gameNumber);
        this.time = time;
        this.userName = userName;
    }

    @SuppressWarnings("unused")
    public GameDefinition getGameDefinition() {
        return gameDefinition;
    }

    @SuppressWarnings("unused")
    public long getTime() {
        return time;
    }

    @SuppressWarnings("unused")
    public String getUserName() {
        return userName;
    }
}
