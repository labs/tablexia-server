package cz.nic.tablexia.server.model.statistics;

@SuppressWarnings("unused")
public class FormValues {

    private long                    minStartTime;
    private long                    maxStartTime;
    private int                     minAge;
    private int                     maxAge;
    private int[]                   gameNumber;
    private int                     minDifficultyNumber;
    private int                     maxDifficultyNumber;
    private int                     genderNumber;
    private int                     completeNumber;
    private int[]                   localeNumber;
    private Long[]                  applicationVersionCode;
    private String[]                userId;
    private String[]                hwSerialNumber;
    private boolean                 validateGames;

    public void setMinStartTime(long minStartTime) {
        this.minStartTime = minStartTime;
    }

    public void setMaxStartTime(long maxStartTime) {
        this.maxStartTime = maxStartTime;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public void setGameNumber(int[] gameNumbers) {
        this.gameNumber = gameNumbers;
    }

    public void setMinDifficultyNumber(int minDifficultyNumber) {
        this.minDifficultyNumber = minDifficultyNumber;
    }

    public void setMaxDifficultyNumber(int maxDifficultyNumber) {
        this.maxDifficultyNumber = maxDifficultyNumber;
    }

    public void setGenderNumber(int genderNumber) {
        this.genderNumber = genderNumber;
    }

    public void setCompleteNumber(int completeNumber) {
        this.completeNumber = completeNumber;
    }

    public void setLocaleNumber(int[] localeNumbers) {
        this.localeNumber = localeNumbers;
    }

    public Long[] getApplicationVersionCode() {
        return applicationVersionCode;
    }

    public void setApplicationVersionCode(Long[] applicationVersionCode) {
        this.applicationVersionCode = applicationVersionCode;
    }

    public long getMinStartTime() {
        return minStartTime;
    }

    public long getMaxStartTime() {
        return maxStartTime;
    }

    public int getMinAge() {
        return minAge;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public int[] getGameNumber() {
        return gameNumber;
    }

    public int getMinDifficultyNumber() {
        return minDifficultyNumber;
    }

    public int getMaxDifficultyNumber() {
        return maxDifficultyNumber;
    }

    public int getGenderNumber() {
        return genderNumber;
    }

    public int getCompleteNumber() {
        return completeNumber;
    }

    public int[] getLocaleNumber() {
        return localeNumber;
    }

    public void setUserId(String[] userId) {
        this.userId = userId;
    }

    public String[] getUserId() {
        return userId;
    }

    public void setHwSerialNumber(String[] hwSerialNumber) {
        this.hwSerialNumber = hwSerialNumber;
    }

    public String[] getHwSerialNumber() {
        return hwSerialNumber;
    }

    public void setValidateGames(boolean validateGames) {
        this.validateGames = validateGames;
    }

    public boolean isValidateGames() {
        return validateGames;
    }
}