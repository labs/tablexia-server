package cz.nic.tablexia.server.model.statistics;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import cz.nic.tablexia.server.dao.StatisticsDAO;

public class GeneralInfo {

    public static class GeneralInfoMapper implements ResultSetMapper<GeneralInfo> {
        @Override
        public GeneralInfo map(int i, ResultSet rs, StatementContext sc) throws SQLException {
            return new GeneralInfo(  rs.getInt(StatisticsDAO.PLAYED_GAMES_COUNT_COLUMN_NAME),
                    rs.getInt(StatisticsDAO.COMPLETE_GAMES_COUNT_COLUMN_NAME),
                    rs.getInt(StatisticsDAO.USERS_COUNT_COLUMN_NAME));
        }
    }

    private int             playedGames;
    private int             completeGames;
    private int             users;

    public GeneralInfo(int playedGames, int completeGames, int users) {
        this.playedGames = playedGames;
        this.completeGames = completeGames;
        this.users = users;
    }

    @SuppressWarnings("unused")
    public int getPlayedGames() {
        return playedGames;
    }

    @SuppressWarnings("unused")
    public int getCompleteGames() {
        return completeGames;
    }

    @SuppressWarnings("unused")
    public int getUsers() {
        return users;
    }
}
