package cz.nic.tablexia.server.model.statistics;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import cz.nic.tablexia.server.dao.StatisticsDAO;

public class FormPreparedValue {

    public static class ApplicationVersionMapper implements ResultSetMapper<FormPreparedValue> {
        @Override
        public FormPreparedValue map(int i, ResultSet rs, StatementContext sc) throws SQLException {
                return new FormPreparedValue(
                        rs.getString(StatisticsDAO.APPLICATION_VERSION_NAME_COLUMN_NAME),
                        rs.getInt(StatisticsDAO.APPLICATION_VERSION_CODE_COLUMN_NAME));
        }
    }

    private String  name;
    private int     value;

    public FormPreparedValue(String name, int value) {
        this.name   = name;
        this.value  = value;
    }

    @SuppressWarnings("unused")
    public String getName() {
        return name;
    }

    @SuppressWarnings("unused")
    public int getValue() {
        return value;
    }
}
