package cz.nic.tablexia.server.model.statistics;

import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameDefinition;

/**
 * Created by drahomir on 11/8/16.
 */
public class ServerOptimizedGame extends Game {
    private Long gameDuration;

    public ServerOptimizedGame(GameDefinition gameDefinition, DifficultyDefinition difficultyDefinition, long duration) {
        super(0L, null, difficultyDefinition.number(), gameDefinition.number(), 0L, 0L, 0L, 0, null, 0, null, 0, 0, 0, null, 0, 0, Game.DEFAULT_USER_RANK, Game.DEFAULT_USER_AGE);
        this.gameDuration = duration;
    }

    @Override
    public Long getGameDuration() {
        return gameDuration;
    }
}