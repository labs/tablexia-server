package cz.nic.tablexia.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class RestServerConfiguration extends Configuration {

    @NotEmpty
    private String secretKey;
    private int secretTimeTolerance = 10;

    @JsonProperty
    public String getSecretKey() {
        return secretKey;
    }

    @JsonProperty
    public int getSecretTimeTolerance() {
        return secretTimeTolerance;
    }

    @Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();

    @JsonProperty("database")
    public void setDataSourceFactory(DataSourceFactory factory) {
        this.database = factory;
    }

    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
        return database;
    }
}
