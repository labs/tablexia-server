package cz.nic.tablexia.server.resources;

import cz.nic.tablexia.shared.security.SecurityRestPath;
import io.dropwizard.testing.junit.ResourceTestRule;

import java.io.ByteArrayInputStream;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import javax.ws.rs.core.Response;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.ClassRule;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.spy;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class SecurityResourceTest {

    private static final SecurityResource SECURITY_RESOURCE = spy(new SecurityResource());
    private static final long SYSTEM_TIME = System.currentTimeMillis();

    @ClassRule
    public static final ResourceTestRule RESOURCES = ResourceTestRule.builder()
            .addResource(SECURITY_RESOURCE)
            .build();

    @Before
    public void setUp() {
        when(SECURITY_RESOURCE.getCurrentTime()).thenReturn(SYSTEM_TIME);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getTime method, of class SecurityResource.
     */
    @Test
    public void testGetTime() {
        Response response = RESOURCES.client().target(SecurityRestPath.SECURITY_PATH + SecurityRestPath.SECURITY_GET).request().post(null);

        ByteArrayInputStream in = (ByteArrayInputStream)response.getEntity();
        int n = in.available();
        byte[] bytes = new byte[n];
        in.read(bytes, 0, n);
        String responseTime = new String(bytes, StandardCharsets.UTF_8);

        assertThat(response.getStatus()).isEqualTo(HttpURLConnection.HTTP_OK);
        assertThat(responseTime).isEqualTo(String.valueOf(SYSTEM_TIME));
    }
}
