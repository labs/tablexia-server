package cz.nic.tablexia.server.dao.mappers;

import cz.nic.tablexia.shared.model.GamePause;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.skife.jdbi.v2.StatementContext;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class GamePauseMapperTest {

    private static final ResultSet RESULT_SET = mock(ResultSet.class);

    private static final long ID            = 12345;
    private static final long START_TIME    = 8887;
    private static final long END_TIME      = 2223;

    @Before
    public void setUp() throws SQLException {
        when(RESULT_SET.getLong("id")).thenReturn(ID);
        when(RESULT_SET.getLong("start_time")).thenReturn(START_TIME);
        when(RESULT_SET.getLong("end_time")).thenReturn(END_TIME);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of map method, of class GamePauseMapper.
     * @throws java.lang.Exception
     */
    @Test
    public void testMap() throws Exception {
        int i = 0;
        StatementContext sc = null;
        GamePauseMapper instance = new GamePauseMapper();
        GamePause result = instance.map(i, RESULT_SET, sc);
        assertEquals(ID, result.getId());
        assertEquals(START_TIME, (long)result.getStartTime());
        assertEquals(END_TIME, (long)result.getEndTime());
    }
}
