package cz.nic.tablexia.server.dao.mappers;

import cz.nic.tablexia.shared.model.Screen;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.skife.jdbi.v2.StatementContext;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class ScreenMapperTest {

    private static final ResultSet RESULT_SET = mock(ResultSet.class);

    private static final long ID        = 12345;
    private static final long USER_ID   = 456789;
    private static final String SCREEN  = "SomeScreen";
    private static final long TIME      = System.currentTimeMillis();

    @Before
    public void setUp() throws SQLException {
        when(RESULT_SET.getString("screen_name")).thenReturn(SCREEN);
        when(RESULT_SET.getLong("time")).thenReturn(TIME);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of map method, of class ScreenMapper.
     * @throws java.lang.Exception
     */
    @Test
    public void testMap() throws Exception {
        int i = 0;
        StatementContext sc = null;
        ScreenMapper instance = new ScreenMapper();
        Screen result = instance.map(i, RESULT_SET, sc);
        assertNotEquals(ID, result.getId());
        assertNotEquals(USER_ID, result.getUserId());

        assertEquals(0, result.getId());
        assertEquals(0, result.getUserId());
        assertEquals(SCREEN, result.getScreenName());
        assertEquals(TIME, result.getTime());
    }
}
