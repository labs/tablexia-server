package cz.nic.tablexia.server.dao.mappers;

import cz.nic.tablexia.shared.model.GameScore;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.skife.jdbi.v2.StatementContext;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class GameScoreMapperTest {

    private static final ResultSet RESULT_SET = mock(ResultSet.class);

    private static final String KEY1 = "score1";
    private static final String VALUE1 = "100";

    @Before
    public void setUp() throws SQLException {
        when(RESULT_SET.getString("key")).thenReturn(KEY1);
        when(RESULT_SET.getString("value")).thenReturn(VALUE1);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of map method, of class GameScoreMapper.
     * @throws java.lang.Exception
     */
    @Test
    public void testMap() throws Exception {
        int i = 0;
        StatementContext sc = null;
        GameScoreMapper instance = new GameScoreMapper();
        GameScore result = instance.map(i, RESULT_SET, sc);
        assertEquals(KEY1, result.getKey());
        assertEquals(VALUE1, result.getValue());
    }
}
