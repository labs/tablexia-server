package cz.nic.tablexia.server.auth;

import cz.nic.tablexia.shared.security.SecurityHelper;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class DigestAuthenticatorTest {

    private static final String KEY = "secret";

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of authenticate method, of class DigestAuthenticator.
     *
     * Wrong key
     * @throws java.lang.Exception
     */
    @Test
    public void testAuthenticateFailKey() throws Exception {
        // digest should have crypted key
        Digest credentials = new Digest(KEY, System.currentTimeMillis());
        DigestAuthenticator instance = new DigestAuthenticator(KEY, 5);

        Optional<Authorized> expResult = Optional.empty();
        Optional<Authorized> result = instance.authenticate(credentials);
        assertEquals(expResult, result);
    }

    /**
     * Test of authenticate method, of class DigestAuthenticator.
     *
     * Time out of tolerance
     * @throws java.lang.Exception
     */
    @Test
    public void testAuthenticateFailTime() throws Exception {
        Calendar beforeTime = Calendar.getInstance();
        beforeTime.setTime(new Date(System.currentTimeMillis()));
        // ten minutes old time
        beforeTime.add(Calendar.MINUTE, -10);
        long time = beforeTime.getTimeInMillis();

        String key = SecurityHelper.getSecretHash(KEY, time);
        Digest credentials = new Digest(key, time);
        // we accept only 5 minutes tolerance
        DigestAuthenticator instance = new DigestAuthenticator(KEY, 5);

        Optional<Authorized> expResult = Optional.empty();
        Optional<Authorized> result = instance.authenticate(credentials);
        assertEquals(expResult, result);
    }

    /**
     * Test of authenticate method, of class DigestAuthenticator.
     *
     * Success key is same and time is in tolerance
     * @throws java.lang.Exception
     */
    @Test
    public void testAuthenticateSuccess() throws Exception {
        Calendar beforeTime = Calendar.getInstance();
        beforeTime.setTime(new Date(System.currentTimeMillis()));
        // time two minutes in future - still in tolerance
        beforeTime.add(Calendar.MINUTE, 2);
        long time = beforeTime.getTimeInMillis();

        String key = SecurityHelper.getSecretHash(KEY, time);
        Digest credentials = new Digest(key, time);
        DigestAuthenticator instance = new DigestAuthenticator(KEY, 5);

        Optional<Authorized> expResult = Optional.of(new Authorized());
        Optional<Authorized> result = instance.authenticate(credentials);
        assertEquals(expResult, result);
    }
}
