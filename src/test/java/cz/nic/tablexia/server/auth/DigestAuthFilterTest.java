package cz.nic.tablexia.server.auth;

import cz.nic.tablexia.shared.security.SecurityRestHeader;
import io.dropwizard.auth.AuthFilter;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.UnauthorizedHandler;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class DigestAuthFilterTest {

    @Mock private ContainerRequestContext context;
    @Mock(name = "unauthorizedHandler") private UnauthorizedHandler handler;
    @Mock(name = "authenticator") private DigestAuthenticator authenticator;
    @InjectMocks private AuthFilter instance;

    private static final String KEY = "test";
    private static final long TIME = 123456;

    ArgumentCaptor<Digest> argument = ArgumentCaptor.forClass(Digest.class);

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() throws AuthenticationException {
        instance = spy(new DigestAuthFilter());
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() {
        reset(context);
    }

    /**
     * Test of filter method, of class DigestAuthFilter.
     * wrong parameters from client
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testFilterWrongParameter1() throws Exception {
        when(context.getHeaderString(SecurityRestHeader.HEADER_AUTH_KEY)).thenReturn(null);
        when(context.getHeaderString(SecurityRestHeader.HEADER_AUTH_TIME)).thenReturn("1234");

        exception.expect(WebApplicationException.class);
        instance.filter(context);
    }

    /**
     * Test of filter method, of class DigestAuthFilter.
     * wrong parameters from client
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testFilterWrongParameter2() throws Exception {
        when(context.getHeaderString(SecurityRestHeader.HEADER_AUTH_KEY)).thenReturn(null);
        when(context.getHeaderString(SecurityRestHeader.HEADER_AUTH_TIME)).thenReturn(null);

        exception.expect(WebApplicationException.class);
        instance.filter(context);
    }

    /**
     * Test of filter method, of class DigestAuthFilter.
     * wrong parameters from client
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testFilterWrongParameter3() throws Exception {
        when(context.getHeaderString(SecurityRestHeader.HEADER_AUTH_KEY)).thenReturn("aaaa");
        when(context.getHeaderString(SecurityRestHeader.HEADER_AUTH_TIME)).thenReturn(null);

        exception.expect(WebApplicationException.class);
        instance.filter(context);
    }

    /**
     * Test of filter method, of class DigestAuthFilter.
     * wrong parameters from client
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testFilterWrongParameter4() throws Exception {
        when(context.getHeaderString(SecurityRestHeader.HEADER_AUTH_KEY)).thenReturn("aaaa");
        when(context.getHeaderString(SecurityRestHeader.HEADER_AUTH_TIME)).thenReturn("");

        exception.expect(WebApplicationException.class);
        instance.filter(context);
    }

    /**
     * Test of filter method, of class DigestAuthFilter.
     * Wrong authentication
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testFilterFailed() throws Exception {
        when(context.getHeaderString(SecurityRestHeader.HEADER_AUTH_KEY)).thenReturn(KEY);
        when(context.getHeaderString(SecurityRestHeader.HEADER_AUTH_TIME)).thenReturn(String.valueOf(TIME));

        // return not authorized
        Mockito.doReturn(Optional.empty()).when(authenticator).authenticate(argument.capture());

        exception.expect(WebApplicationException.class);
        instance.filter(context);

        verify(authenticator).authenticate(argument.capture());
        assertEquals(KEY, argument.getValue().getHash());
        assertEquals(TIME, argument.getValue().getTime());
    }

    /**
     * Test of filter method, of class DigestAuthFilter.
     * Wrong authentication
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testFilterSuccess() throws Exception {
        when(context.getHeaderString(SecurityRestHeader.HEADER_AUTH_KEY)).thenReturn(KEY);
        when(context.getHeaderString(SecurityRestHeader.HEADER_AUTH_TIME)).thenReturn(String.valueOf(TIME));

        // return authorized
        Mockito.doReturn(Optional.of(new Authorized())).when(authenticator).authenticate(argument.capture());

        // user was authorized - no exception should raise here
        instance.filter(context);

        assertEquals(KEY, argument.getValue().getHash());
        assertEquals(TIME, argument.getValue().getTime());
    }
}
