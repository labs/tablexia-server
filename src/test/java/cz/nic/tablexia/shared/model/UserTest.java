package cz.nic.tablexia.shared.model;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;

import java.util.ArrayList;

import cz.nic.tablexia.shared.model.definitions.GenderDefinition;
import io.dropwizard.jackson.Jackson;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class UserTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Test
    public void serializesToJSON() throws Exception {
        final User user = new User(1, "Kolomaz", 12, GenderDefinition.MALE, "5", "[[{class:cz.nic.tablexia.util.Point,x:0.32175,y:0.45841277},{class:cz.nic.tablexia.util.Point,x:0.31999218,y:0.453443},{class:cz.nic.tablexia.util.Point,x:0.31647655,y:0.4435034},{class:cz.nic.tablexia.util.Point,x:0.31120312,y:0.42859408},{class:cz.nic.tablexia.util.Point,x:0.30417186,y:0.40871492},{class:cz.nic.tablexia.util.Point,x:0.3014863,y:0.3982784},{class:cz.nic.tablexia.util.Point,x:0.30314648,y:0.39728442},{class:cz.nic.tablexia.util.Point,x:0.30915233,y:0.40573308},{class:cz.nic.tablexia.util.Point,x:0.3195039,y:0.42362428},{class:cz.nic.tablexia.util.Point,x:0.32892773,y:0.44250947},{class:cz.nic.tablexia.util.Point,x:0.33742383,y:0.4623886},{class:cz.nic.tablexia.util.Point,x:0.3449922,y:0.4832617},{class:cz.nic.tablexia.util.Point,x:0.3516328,y:0.5051288},{class:cz.nic.tablexia.util.Point,x:0.35807812,y:0.5272443},{class:cz.nic.tablexia.util.Point,x:0.36432812,y:0.5496084},{class:cz.nic.tablexia.util.Point,x:0.37038282,y:0.5722209},{class:cz.nic.tablexia.util.Point,x:0.3", false, false, false);
        user.setUuid("1234");
        user.setGames(new ArrayList<Game>());

        final String expected = MAPPER.writeValueAsString(
                 MAPPER.readValue(fixture("fixtures/user.json"), User.class));

         assertThat(MAPPER.writeValueAsString(user)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        final User user = new User(1, "Kolomaz", 12, GenderDefinition.MALE, "5", "[[{class:cz.nic.tablexia.util.Point,x:0.32175,y:0.45841277},{class:cz.nic.tablexia.util.Point,x:0.31999218,y:0.453443},{class:cz.nic.tablexia.util.Point,x:0.31647655,y:0.4435034},{class:cz.nic.tablexia.util.Point,x:0.31120312,y:0.42859408},{class:cz.nic.tablexia.util.Point,x:0.30417186,y:0.40871492},{class:cz.nic.tablexia.util.Point,x:0.3014863,y:0.3982784},{class:cz.nic.tablexia.util.Point,x:0.30314648,y:0.39728442},{class:cz.nic.tablexia.util.Point,x:0.30915233,y:0.40573308},{class:cz.nic.tablexia.util.Point,x:0.3195039,y:0.42362428},{class:cz.nic.tablexia.util.Point,x:0.32892773,y:0.44250947},{class:cz.nic.tablexia.util.Point,x:0.33742383,y:0.4623886},{class:cz.nic.tablexia.util.Point,x:0.3449922,y:0.4832617},{class:cz.nic.tablexia.util.Point,x:0.3516328,y:0.5051288},{class:cz.nic.tablexia.util.Point,x:0.35807812,y:0.5272443},{class:cz.nic.tablexia.util.Point,x:0.36432812,y:0.5496084},{class:cz.nic.tablexia.util.Point,x:0.37038282,y:0.5722209},{class:cz.nic.tablexia.util.Point,x:0.3", false, false, false);
        user.setUuid("1234");
        user.setGames(new ArrayList<Game>());

        assertThat(MAPPER.readValue(fixture("fixtures/user.json"), User.class))
                .isEqualTo(user);
    }
}
